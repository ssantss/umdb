import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import Toasted from 'vue-toasted';

Vue.config.productionTip = false
Vue.use(Toasted);

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
